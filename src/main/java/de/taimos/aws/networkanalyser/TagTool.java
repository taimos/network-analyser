package de.taimos.aws.networkanalyser;

import java.util.List;

import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Tag;

/**
 * Copyright 2013 Cinovo AG<br>
 * <br>
 * 
 * @author thoeger
 * 
 */
public class TagTool {
	
	/**
	 * @param tags the tags
	 * @param def the default value
	 * @return the name
	 */
	public String getName(List<Tag> tags, String def) {
		String name = AWS.getNameTag(tags);
		if ((name == null) || name.isEmpty()) {
			return def;
		}
		return name;
	}
	
	/**
	 * @param tags the tags
	 * @return the name
	 */
	public String getName(List<Tag> tags) {
		return this.getName(tags, null);
	}
	
	/**
	 * @param tags the tags
	 * @return the area
	 */
	public String getArea(List<Tag> tags) {
		return AWS.getAreaTag(tags);
	}
	
	/**
	 * @param subnetId the subnet id
	 * @return the subnet label
	 */
	public String getSubnetLabel(String subnetId) {
		Subnet subnet = Starter.subnets.get(subnetId);
		if (subnet == null) {
			return "N/A";
		}
		return String.format("%s (%s)", subnet.getSubnetId(), this.getName(subnet.getTags(), ""));
	}
	
}
