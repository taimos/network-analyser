package de.taimos.aws.networkanalyser.model;

public class IpPerm {
	
	private String protocol;
	private String source;
	
	
	/**
	 * @return the protocol
	 */
	public String getProtocol() {
		return this.protocol;
	}
	
	/**
	 * @param protocol the protocol to set
	 */
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	
	/**
	 * @return the source
	 */
	public String getSource() {
		return this.source;
	}
	
	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
	
}
