package de.taimos.aws.networkanalyser;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.UserIdGroupPair;

import de.taimos.aws.networkanalyser.model.IpPerm;

public class PermissionTool {
	
	/**
	 * @param permissions permission to convert
	 * @return teh converted permission
	 */
	public List<IpPerm> convert(List<IpPermission> permissions) {
		List<IpPerm> res = new ArrayList<>();
		for (IpPermission p : permissions) {
			
			String prot;
			if (p.getIpProtocol().equals("-1")) {
				prot = "any";
			} else {
				prot = p.getIpProtocol();
			}
			
			if (((p.getFromPort() == null) && (p.getToPort() == null)) || (p.getFromPort().equals(-1) && p.getToPort().equals(-1))) {
				prot += ": any";
			} else {
				prot += String.format(": %s-%s", p.getFromPort(), p.getToPort());
			}
			
			List<String> ipRanges = p.getIpRanges();
			for (String s : ipRanges) {
				IpPerm r = new IpPerm();
				r.setProtocol(prot);
				r.setSource(s);
				res.add(r);
			}
			List<UserIdGroupPair> groupPairs = p.getUserIdGroupPairs();
			for (UserIdGroupPair gp : groupPairs) {
				IpPerm r = new IpPerm();
				r.setProtocol(prot);
				r.setSource(String.format("%s (%s)", Starter.securityGroups.get(gp.getGroupId()).getGroupName(), gp.getGroupId()));
				res.add(r);
			}
		}
		return res;
	}
}
