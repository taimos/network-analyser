package de.taimos.aws.networkanalyser;

import java.io.FileWriter;
import java.io.StringWriter;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.log.Log4JLogChute;

/**
 * Copyright 2013 Cinovo AG<br>
 * <br>
 * 
 * @author thoeger
 * 
 */
public class VelocityBodyWriter {
	
	static {
		try {
			// Use ClasspathLoader
			Velocity.setProperty(RuntimeConstants.RESOURCE_LOADER, "class");
			Velocity.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
			// Use UTF-8
			Velocity.setProperty("input.encoding", "UTF-8");
			Velocity.setProperty("output.encoding", "UTF-8");
			// Use log4j
			Velocity.setProperty(RuntimeConstants.RUNTIME_LOG_LOGSYSTEM_CLASS, Log4JLogChute.class.getCanonicalName());
			Velocity.setProperty("runtime.log.logsystem.log4j.logger", "org.apache.velocity");
			Velocity.init();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private static String evaluateVM(final String name, final Map<String, Object> variables) {
		/* lets make a Context and put data into it */
		final VelocityContext context = new VelocityContext();
		
		final Set<Entry<String, Object>> entrySet = variables.entrySet();
		for (final Entry<String, Object> entry : entrySet) {
			context.put(entry.getKey(), entry.getValue());
		}
		
		final Template template = Velocity.getTemplate(name);
		final StringWriter w = new StringWriter();
		template.merge(context, w);
		return w.toString();
	}
	
	/**
	 * @param file the target file
	 * @param name the template name
	 * @param variables the variables
	 */
	public static void write(String file, final String name, final Map<String, Object> variables) {
		final String evaluate = VelocityBodyWriter.evaluateVM("templates/" + name, variables);
		try (FileWriter fw = new FileWriter(file)) {
			fw.write(evaluate);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}