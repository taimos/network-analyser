package de.taimos.aws.networkanalyser;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2Client;
import com.amazonaws.services.ec2.model.RouteTable;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.Vpc;

/**
 * Copyright 2013 Cinovo AG<br>
 * <br>
 * 
 * @author thoeger
 * 
 */
public class AWS {
	
	public static Map<String, SecurityGroup> getSecurityGroups() {
		List<SecurityGroup> list = AWS.createClient().describeSecurityGroups().getSecurityGroups();
		Map<String, SecurityGroup> res = new HashMap<String, SecurityGroup>();
		for (SecurityGroup sg : list) {
			res.put(sg.getGroupId(), sg);
		}
		return res;
	}
	
	public static Map<String, Subnet> getSubnets() {
		List<Subnet> list = AWS.createClient().describeSubnets().getSubnets();
		Map<String, Subnet> map = new HashMap<>();
		for (Subnet s : list) {
			map.put(s.getSubnetId(), s);
		}
		return map;
	}
	
	public static Map<String, RouteTable> getRouteTables() {
		List<RouteTable> list = AWS.createClient().describeRouteTables().getRouteTables();
		Map<String, RouteTable> map = new HashMap<>();
		for (RouteTable rt : list) {
			map.put(rt.getRouteTableId(), rt);
		}
		return map;
	}
	
	public static Map<String, Vpc> getVPC() {
		List<Vpc> vpcs = AWS.createClient().describeVpcs().getVpcs();
		Map<String, Vpc> map = new HashMap<>();
		for (Vpc vpc : vpcs) {
			map.put(vpc.getVpcId(), vpc);
		}
		return map;
	}
	
	public static String getNameTag(List<Tag> tags) {
		return AWS.getTag("Name", tags);
	}
	
	public static String getAreaTag(List<Tag> tags) {
		return AWS.getTag("area", tags);
	}
	
	public static String getTag(String key, List<Tag> tags) {
		for (Tag tag : tags) {
			if (tag.getKey().equals(key)) {
				return tag.getValue();
			}
		}
		return null;
	}
	
	private static AmazonEC2Client createClient() {
		AmazonEC2Client cl;
		if ((Starter.ACCESS_KEY != null) && (Starter.SECRET_KEY != null)) {
			cl = new AmazonEC2Client(new BasicAWSCredentials(Starter.ACCESS_KEY, Starter.SECRET_KEY));
		} else {
			cl = new AmazonEC2Client();
		}
		cl.setRegion(Region.getRegion(Regions.fromName(Starter.REGION)));
		return cl;
	}
}
