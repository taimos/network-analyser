package de.taimos.aws.networkanalyser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import com.amazonaws.services.ec2.model.RouteTable;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.Subnet;
import com.amazonaws.services.ec2.model.Vpc;

/**
 * Copyright 2013 Cinovo AG<br>
 * <br>
 * 
 * @author thoeger
 * 
 */
public class Starter {
	
	public static String ACCESS_KEY = null;
	public static String SECRET_KEY = null;
	public static String REGION = "eu-west-1";
	
	public static Map<String, SecurityGroup> securityGroups;
	public static Map<String, Subnet> subnets;
	public static Map<String, Vpc> vpc;
	public static Map<String, RouteTable> routeTables;
	
	
	/**
	 * @param args -
	 */
	public static void main(String[] args) {
		OptionParser p = new OptionParser();
		OptionSpec<String> keyOpt = p.accepts("key", "the AWS access key").withRequiredArg().ofType(String.class);
		OptionSpec<String> secretOpt = p.accepts("secret", "the AWS secret key").requiredIf("key").withRequiredArg().ofType(String.class);
		OptionSpec<String> regionOpt = p.accepts("region", "the AWS region (default eu-west-1)").withRequiredArg().ofType(String.class);
		OptionSpec<String> fileOpt = p.accepts("file", "the target file (default network.html)").withRequiredArg().ofType(String.class);
		p.accepts("help", "show usage");
		
		OptionSet options;
		try {
			options = p.parse(args);
		} catch (OptionException e1) {
			options = null;
		}
		
		if ((options == null) || options.has("help")) {
			try {
				p.printHelpOn(System.out);
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.exit(1);
			// compiler doesn't understand exit(1)
			throw new RuntimeException();
		}
		
		if (options.has(keyOpt) && options.has(secretOpt)) {
			Starter.ACCESS_KEY = options.valueOf(keyOpt);
			Starter.SECRET_KEY = options.valueOf(secretOpt);
		}
		if (options.has(regionOpt)) {
			Starter.REGION = options.valueOf(regionOpt);
		}
		
		Starter.securityGroups = AWS.getSecurityGroups();
		Starter.subnets = AWS.getSubnets();
		Starter.vpc = AWS.getVPC();
		Starter.routeTables = AWS.getRouteTables();
		
		final String targetFile;
		if (options.has(fileOpt)) {
			targetFile = options.valueOf(fileOpt);
		} else {
			targetFile = "network.html";
		}
		Starter.renderHTML(targetFile);
	}
	
	private static void renderHTML(String target) {
		Map<String, Object> vars = new HashMap<>();
		vars.put("tt", new TagTool());
		vars.put("pt", new PermissionTool());
		
		vars.put("sgs", Starter.sortSecurityGroups());
		vars.put("subnets", Starter.sortSubnets());
		vars.put("vpcs", Starter.vpc.values());
		vars.put("rtbs", Starter.sortRouteTables());
		
		VelocityBodyWriter.write(target, "report.html", vars);
	}
	
	private static Collection<SecurityGroup> sortSecurityGroups() {
		List<SecurityGroup> values = new ArrayList<>(Starter.securityGroups.values());
		Collections.sort(values, new Comparator<SecurityGroup>() {
			
			@Override
			public int compare(SecurityGroup o1, SecurityGroup o2) {
				return o1.getGroupName().compareTo(o2.getGroupName());
			}
		});
		return values;
	}
	
	private static Collection<Subnet> sortSubnets() {
		List<Subnet> values = new ArrayList<>(Starter.subnets.values());
		Collections.sort(values, new Comparator<Subnet>() {
			
			@Override
			public int compare(Subnet o1, Subnet o2) {
				String name1 = AWS.getNameTag(o1.getTags());
				String name2 = AWS.getNameTag(o2.getTags());
				if (name1 == null) {
					if (name2 == null) {
						return o1.getSubnetId().compareTo(o2.getSubnetId());
					}
					return -1;
				}
				if (name2 == null) {
					return 1;
				}
				return name1.compareTo(name2);
			}
		});
		return values;
	}
	
	private static Collection<RouteTable> sortRouteTables() {
		List<RouteTable> values = new ArrayList<>(Starter.routeTables.values());
		Collections.sort(values, new Comparator<RouteTable>() {
			
			@Override
			public int compare(RouteTable o1, RouteTable o2) {
				String name1 = AWS.getNameTag(o1.getTags());
				String name2 = AWS.getNameTag(o2.getTags());
				if (name1 == null) {
					if (name2 == null) {
						return o1.getRouteTableId().compareTo(o2.getRouteTableId());
					}
					return -1;
				}
				if (name2 == null) {
					return 1;
				}
				return name1.compareTo(name2);
			}
		});
		return values;
	}
}
